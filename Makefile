
blink.hex: blink.asm
	avra blink.asm

hello.hex: hello.asm
	avra hello.asm

hello: hello.hex
	avrdude -p m328p -carduino -D -b 115200 \
		-P /dev/ttyUSB0 -U flash:w:$<

blink: blink.hex
	avrdude -p m328p -carduino -D -b 115200 \
		-P /dev/ttyUSB0 -U flash:w:$<

