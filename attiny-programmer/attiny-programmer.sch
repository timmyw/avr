EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "attinyXX programmer"
Date ""
Rev ""
Comp ""
Comment1 "Author: Tim Whelan (tim@zipt.co)"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Sockets-for-ICs-Transistors:A_08-LC-TT J1
U 1 1 5ED44D3D
P 2250 1500
F 0 "J1" H 2250 1783 50  0000 C CNN
F 1 "A_08-LC-TT" H 2250 1784 50  0001 C CNN
F 2 "digikey-footprints:SOCKET_DIP-8_7.62mm_Conn" H 2450 1700 60  0001 L CNN
F 3 "http://www.assmann-wsw.com/fileadmin/datasheets/ASS_0810_CO.pdf" H 2450 1800 60  0001 L CNN
F 4 "AE9986-ND" H 2450 1900 60  0001 L CNN "Digi-Key_PN"
F 5 "A 08-LC-TT" H 2450 2000 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 2450 2100 60  0001 L CNN "Category"
F 7 "Sockets for ICs, Transistors" H 2450 2200 60  0001 L CNN "Family"
F 8 "http://www.assmann-wsw.com/fileadmin/datasheets/ASS_0810_CO.pdf" H 2450 2300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/assmann-wsw-components/A-08-LC-TT/AE9986-ND/821740" H 2450 2400 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN IC DIP SOCKET 8POS TIN" H 2450 2500 60  0001 L CNN "Description"
F 11 "Assmann WSW Components" H 2450 2600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2450 2700 60  0001 L CNN "Status"
	1    2250 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5ED472F6
P 2700 2000
F 0 "C1" V 2850 2000 50  0000 C CNN
F 1 "4.7uF" V 2536 2000 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 2738 1850 50  0001 C CNN
F 3 "~" H 2700 2000 50  0001 C CNN
	1    2700 2000
	0    1    1    0   
$EndComp
Text Label 3450 1300 0    50   ~ 0
GND
Text Label 3450 1800 0    50   ~ 0
RESET
Text Label 3450 1200 0    50   ~ 0
5V
Text Label 3450 1400 0    50   ~ 0
Pin13
Wire Wire Line
	2500 1350 2500 1200
Wire Wire Line
	2500 1200 3200 1200
Wire Wire Line
	2500 1450 2700 1450
Wire Wire Line
	2700 1450 2700 1400
Wire Wire Line
	2700 1400 3200 1400
Wire Wire Line
	3200 1300 3050 1300
Wire Wire Line
	3050 1300 3050 1100
Wire Wire Line
	3050 1100 1850 1100
Wire Wire Line
	1850 1100 1850 1650
Wire Wire Line
	1850 1650 2000 1650
Wire Wire Line
	1850 1650 1850 2000
Wire Wire Line
	1850 2000 2550 2000
Connection ~ 1850 1650
Text Label 3450 1500 0    50   ~ 0
Pin12
Wire Wire Line
	3200 1500 2750 1500
Wire Wire Line
	2750 1500 2750 1550
Wire Wire Line
	2750 1550 2500 1550
Text Label 3450 1600 0    50   ~ 0
Pin11
Wire Wire Line
	3200 1600 2800 1600
Wire Wire Line
	2800 1600 2800 1650
Wire Wire Line
	2800 1650 2500 1650
$Comp
L Connector:Conn_01x07_Male J2
U 1 1 5ED642CC
P 3400 1500
F 0 "J2" H 3450 1900 50  0000 R CNN
F 1 "Conn_01x07_Male" H 3508 1890 50  0001 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Horizontal" H 3400 1500 50  0001 C CNN
F 3 "~" H 3400 1500 50  0001 C CNN
	1    3400 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 2000 2850 1800
Wire Wire Line
	2850 1800 3200 1800
Text Label 3450 1700 0    50   ~ 0
Pin10
Wire Wire Line
	3200 1700 2950 1700
Wire Wire Line
	2950 1700 2950 1000
Wire Wire Line
	2950 1000 1900 1000
Wire Wire Line
	1900 1000 1900 1350
Wire Wire Line
	1900 1350 2000 1350
NoConn ~ 2000 1450
NoConn ~ 2000 1550
$Comp
L Device:LED D1
U 1 1 5ED3A193
P 1500 1200
F 0 "D1" H 1493 1325 50  0000 C CNN
F 1 "LED" H 1493 1325 50  0001 C CNN
F 2 "" H 1500 1200 50  0001 C CNN
F 3 "~" H 1500 1200 50  0001 C CNN
	1    1500 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1200 2500 1200
Connection ~ 2500 1200
$Comp
L Device:R R1
U 1 1 5ED3BFD2
P 1200 1450
F 0 "R1" V 1200 1400 50  0000 L CNN
F 1 "330" V 1300 1350 50  0000 L CNN
F 2 "" V 1130 1450 50  0001 C CNN
F 3 "~" H 1200 1450 50  0001 C CNN
	1    1200 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1200 1200 1200
Wire Wire Line
	1200 1200 1200 1300
Wire Wire Line
	1200 1600 1200 1650
Wire Wire Line
	1200 1650 1850 1650
$EndSCHEMATC
