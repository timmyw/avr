.nolist
.include "./tn85def.inc"
.list

.def temp = r16
	
.org 0x0000
	rjmp main

.org 0x020	
main:
 	ldi temp, (1<<DDB3) | (1<<DDB5) ; DDB3 and DDB5 are output, DDB4 are input
 	out DDRB, temp
	
	rcall pulse_latch_out
	rcall pulse_clock
	rcall delay_05
	rjmp main
;; 	ldi temp, (1<<DDB3)
;; 	out DDRB, temp

;; 	sbi PortB, 3
	
;; loop:
;; 	in temp, PinB
;; 	andi temp, (1<<DDB4)
;; 	brne turn_off
;; turn_on:
;; 	sbi PortB, 3
;; 	rjmp loop
;; turn_off:
;; 	cbi PortB, 3
;; 	rjmp loop	

delay_05:
	ldi temp, 8
outer_loop:
 
	;; ldi r24, low(3037)
	;; ldi r25, high(3037)
	ldi r24, low($0)
	ldi r25, high($0)
delay_loop:
	adiw r24, 1
	brne delay_loop
 
	dec temp
	brne outer_loop
	ret	

;; ;;; Read in 8 bits from DDB4.  Uses DDB3 as clock and DDB5 as latch
;; ;;; Destroys r16, r17, r18
;; ;;; The result is in r16
;; serial_read_8:
;; 	ldi temp, (1<<DDB3) | (1<<DDB5) ; DDB3 and DDB5 are output, DDB4 are input
;; 	out DDRB, temp
;; 	;; Pulse latch high
;; 	ldi temp, DDB5
;; 	rcall pulse_pin

;; 	;; Loop for 8 bits:
;; 	ldi r18, 8 		; 8 bits
;; sr_8_loop:
;; 	;; Pulse clock pin
;; 	;; Read DDB4
;; 	;; Build r17
;; 	;; Dec the loop and jump back
;; 	dec r18
;; 	brne sr_8_loop
;; 	ret

;;; Pulse the latch pin (DDB5)
pulse_latch_out:
	;; Set DDB5 pin high
	sbi PortB, 5
	;; Delay
	rcall delay_20_ms
	;; Set pin low
	cbi PortB, 5
	ret

;;; Pulse the clock pin (DDB3)
pulse_clock:
	;; Set pin high
	sbi PortB, 3
	;; Delay
	rcall delay_10_ms
	;; Set pin low
	cbi PortB, 3
	ret
	
;;; Delay for (approx) 20 milliseconds
delay_20_ms:			; 20ms is 800000 cycles
	ldi temp, 20		; 20 x 1 ms
outer_loop_d20:
 	ldi r24, low(63484)	; Count from 63484 up to 65535
	ldi r25, high(63484)	; which is 2051
delay_loop_d20:			;
	adiw r24, 1 		;
	brne delay_loop_d20	; 
	dec temp
	brne outer_loop_d20
	ret	

;;; Delay for (approx) 10 milliseconds
delay_10_ms:			; 10ms is 400000 cycles
	ldi temp, 10		; 
outer_loop_d10:
 	ldi r24, low(63484)	; Count from 63484 up to 65535
	ldi r25, high(63484)	; which is 2051
delay_loop_d10:			; 4 cycles for the loop
	adiw r24, 1 		;
	brne delay_loop_d10	; 
	;; If we get here we've done 2051*4=8204(ish) cycles
	dec temp
	brne outer_loop_d10
	ret	

;;; M=C/t
;;; 16000 cycles is 1 millisecond
;;; Inner loop is 3 cycles, so loop
;;; 16000/3 =  5333 times
;;; 65535-5333 = 60202

