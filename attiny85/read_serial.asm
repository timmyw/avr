.nolist
.include "./tn85def.inc"
.list
	
.def temp = r16
	
.equ LATCH_OUT = DDB0		; OUT
.equ SER_OUT   = DDB1		; OUT
.equ CLOCK     = DDB2		; OUT
.equ SER_IN    = DDB3		; IN
.equ LATCH_IN  = DDB4		; OUT

.org 0x0000
	rjmp main

.org 0x020	
main:
	;; Set up the port directions
 	ldi temp, (1<<LATCH_IN) | (1<<CLOCK) | (1<<SER_OUT) | (1<<LATCH_OUT)
 	out DDRB, temp
	;; Set the LATCH_IN high
	sbi PortB, LATCH_IN

	
	rcall serial_read_16
	;; We should have bytes in r17 and r18

	mov r16, r18
	rcall seven_write_nibble

	;; Dump both bytes to SER_OUT
	;; mov r16, r18
	;; rcall serial_write_8
	;; mov r16, r17
	;; rcall serial_write_8
	;; rcall delay_10_ms	
	rjmp main

.include "./delay.asm"

seven_font_list:
	.db FN_0, FN_1, FN_2, FN_3, FN_4, FN_5, FN_6, FN_7
	.db FN_8, FN_9, FN_A, FN_B, FN_C, FN_D, FN_E, FN_F

init_data:
	;; Copy the initial values for the coefficient table from
	;; Flash into RAM.
	ldi ZH, high(seven_font_list << 1)
	ldi ZL, low(seven_font_list << 1)
	ldi XH, high(seven_font)
	ldi XL, low(seven_font)
	ldi r16, 16
init_data_loop:
	lpm r17, Z+	
	st X+, r17
	dec r16
	brne init_data_loop
	ret
	
	
;;; Write the lower order nibble in r16 to the output
seven_write_nibble:
	andi r16, $0f
	ldi xh, high(seven_font)
	ldi xl, low(seven_font)
	add xl, r16
	ld  r16, x		; r16 should have the font bitmap
	rcall serial_write_8
	ret
	
;;; Write 8 bits to SER_OUT
;;; Expects the 8 bits in r16
;;; Clobbers: r18, r19 (and anything that the delay loop clobbers)
serial_write_8:
	clr r19			; Shift r16 in r19
	add r19, r16
	ldi r18, 8
sw_8_loop:
	rol r19
	brcc sw_8_zero
sw_8_one:
	sbi PortB, SER_OUT
	rjmp sw_8_done
sw_8_zero:
	cbi PortB, SER_OUT	
	rjmp sw_8_done
sw_8_done:
	;; rcall pulse_clock	
	sbi PortB, CLOCK
	rcall delay_10_ms
	cbi PortB, CLOCK
	rcall delay_10_ms
	dec r18
	brne sw_8_loop
	cbi PortB, SER_OUT

	;;  Pulse the LATCH_OUT pin
	rcall pulse_latch_out
	ret
	
;; ;;; Read in 8 bits from SER_IN.  Uses CLOCK and LATCH_IN
;; ;;; Destroys r16, r17, r18
;; ;;; The result is in r16
serial_read_8:
	rcall pulse_latch_in

	ldi r18, 8 		; 8 bits
	ldi r17, 0		; Clear our building byte
sr_8_loop:
	;; Pulse clock pin
	in r16, PinB
	andi r16, (1<<SER_IN)
	;; Build r17
	brne sr_8_one
sr_8_zero:
	;; cbi PortB, SER_OUT	; TODO
	clc
	rjmp done_set
sr_8_one:
	;; sbi PortB, SER_OUT	; TODO
	sec
	rjmp done_set
done_set:
	;; Rotate r17 for the next bit
	ror r17
	sbi PortB, CLOCK
	rcall delay_10_ms
	cbi PortB, CLOCK
	;; Dec the loop and jump back
	rcall delay_10_ms
	;; cbi PortB, SER_OUT	; TODO
	dec r18
	brne sr_8_loop
	;; Transfer result to r16
	mov r16, r17
	ret

;;; Read in 16 bits from SER_IN.  Uses CLOCK and LATCH_IN
;;; Destroys r16, r17, r18, r19 and r20
;;; The result are in: byte0:r18 and byte1:r17
serial_read_16:

	;; Pulse the latch signal low then high.
	rcall pulse_latch_in

	ldi r18, 0		; This will store the first byte
	;; ldi r20, 1		;We're doing two bytes
	ldi r19, 16 		; 8 bits
	ldi r17, 0		; Clear our building byte

	;; At this point
	;; Q_H should have the first bit
sr_16_loop:
	;; Read SER_IN
	in r16, PinB
	andi r16, (1<<SER_IN)
	;; Build r17
	brne sr_16_one

sr_16_zero: clc
	;; cbi PortB, SER_OUT	; TODO
	rjmp done_16_read

sr_16_one: sec
	;; sbi PortB, SER_OUT	; TODO
	rjmp done_16_read	; Keep the timings the same

done_16_read:
	;; Rotate r17 for the next bit, pulling in C
	ror r17
	;; Now shift the bits
	;; Pulse clock pin
	sbi PortB, CLOCK
	rcall delay_10_ms
	cbi PortB, CLOCK

	;; Dec the loop and jump back
	rcall delay_10_ms
	;; cbi PortB, SER_OUT	; TODO

	;; Shift to next byte if necessary
	cpi r19, 9
	brne sr_16_same_byte

	;; We've finished byte one
	mov r18, r17		; Store the first byte
sr_16_same_byte:	
	dec r19
	brne sr_16_loop
	
	;; We have the first byte in r18 and the second in r17
	ret
	
;;; Pulse the latch pin low and then high again
pulse_latch_in:
	;; Set DDB5 pin low
	cbi PortB, LATCH_IN
	;; Delay
	rcall delay_20_ms
	;; Set pin high
	sbi PortB, LATCH_IN
	ret

;;; Pulse the latch pin low and then high again
pulse_latch_out:
	;; Set DDB5 pin low
	cbi PortB, LATCH_OUT
	;; Delay
	rcall delay_10_ms
	;; Set pin high
	sbi PortB, LATCH_OUT
	ret
	
;;; Pulse the clock pin
pulse_clock:
	;; Set pin high
	sbi PortB, CLOCK
	;; Delay
	rcall delay_10_ms
	;; Set pin low
	cbi PortB, CLOCK
	ret

pulse_clock_up:
	;; Set pin high
	sbi PortB, CLOCK
	ret

pulse_clock_down:
	;; Set pin high
	cbi PortB, CLOCK
	ret
	
;;; Delay for (approx) 20 milliseconds
delay_20_ms:			; 20ms is 800000 cycles
	ldi temp, 20		; 20 x 1 ms
outer_loop_d20:
 	ldi r24, low(63484)	; Count from 63484 up to 65535
	ldi r25, high(63484)	; which is 2051
delay_loop_d20:			;
	adiw r24, 1 		;
	brne delay_loop_d20	; 
	dec temp
	brne outer_loop_d20
	ret	

;;; Delay for (approx) 10 milliseconds
delay_10_ms:			; 10ms is 400000 cycles
	ldi temp, 10		; 
outer_loop_d10:
 	ldi r24, low(63484)	; Count from 63484 up to 65535
	ldi r25, high(63484)	; which is 2051
delay_loop_d10:			; 4 cycles for the loop
	adiw r24, 1 		;
	brne delay_loop_d10	; 
	;; If we get here we've done 2051*4=8204(ish) cycles
	dec temp
	brne outer_loop_d10
	ret	
	
;;; M=C/t
;;; 16000 cycles is 1 millisecond
;;; Inner loop is 3 cycles, so loop
;;; 16000/3 =  5333 times
;;; 65535-5333 = 60202

.include "./7segment.inc"

.dseg
seven_font:	.byte 	$10 
