;;; Mimics SPI to read and write bytes

;;; Write 8 bits to SER_OUT
;;; Expects the 8 bits in r16
;;; Clobbers: r18, r19 (and anything that the delay loop clobbers)
serial_write_8:
	clr r19			; Shift r16 in r19
	add r19, r16
	ldi r18, 8
sw_8_loop:
	rol r19
	brcc sw_8_zero
sw_8_one:
	sbi PortB, SER_OUT
	rjmp sw_8_done
sw_8_zero:
	cbi PortB, SER_OUT	
	rjmp sw_8_done
sw_8_done:
	;; rcall pulse_clock	
	sbi PortB, CLOCK
	rcall delay_10_ms
	cbi PortB, CLOCK
	rcall delay_10_ms
 	dec r18
	brne sw_8_loop
	cbi PortB, SER_OUT

	;;  Pulse the LATCH_OUT pin
	rcall pulse_latch_out
	ret
	
;; ;;; Read in 8 bits from SER_IN.  Uses CLOCK and LATCH_IN
;; ;;; Destroys r16, r17, r18
;; ;;; The result is in r16
serial_read_8:
	rcall pulse_latch_in

	ldi r18, 8 		; 8 bits
	ldi r17, 0		; Clear our building byte
sr_8_loop:
	;; Pulse clock pin
	in r16, PinB
	andi r16, (1<<SER_IN)
	;; Build r17
	brne sr_8_one
sr_8_zero:
	;; cbi PortB, SER_OUT	; TODO
	clc
	rjmp done_set
sr_8_one:
	;; sbi PortB, SER_OUT	; TODO
	sec
	rjmp done_set
done_set:
	;; Rotate r17 for the next bit
	ror r17
	sbi PortB, CLOCK
	rcall delay_10_ms
	cbi PortB, CLOCK
	;; Dec the loop and jump back
	rcall delay_10_ms
	;; cbi PortB, SER_OUT	; TODO
	dec r18
	brne sr_8_loop
	;; Transfer result to r16
	mov r16, r17
	ret

;;; Pulse the clock pin
pulse_clock:
	;; Set pin high
	sbi PortB, CLOCK
	;; Delay
	rcall delay_10_ms
	;; Set pin low
	cbi PortB, CLOCK
	ret

	
