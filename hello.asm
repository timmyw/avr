;hello.asm
;  turns on an LED which is connected to PB5 (digital out 13)

.nolist
.include "./include/m328Pdef.inc"
.list

.def temp =r16

	rjmp start
start:
	ser	temp
	out	DDRB,temp
	ldi	temp,0b11111110
	out	DDRD,temp
	clr	temp
	out	PortB,temp
	ldi	temp,0b11111111
	out	PortD,temp
Main:
	in	temp,PinD
	out	PortB,temp
	rjmp 	Main

